![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [추상 클래스(abstract class)](#추상-클래스)
- [인터페이스(interface)](#인터페이스)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Abstract

## 추상 클래스
- abstract 지정자를 통해 Class 선언
  - 추상 메소드를 반드시 포함해야하지는 않음
  - 추상 메소드가 포함되어 있다면 반드시 추상 클래스로 지정해야함
  - extends로 상속. 상속 시 일반 메소드 외 추상 메소드만 구현
  - Factory Method Pattern 등에서 사용
  
  
```java
abstract class AbstractClass {
    String value;           // 추상 클래스에는 변수 선언 가능

    void absMethod() {
        System.out.println("Hello");
    }
    void absMethod2();      // Error 발생, 메서드 내 Body 필요
}
```



## 인터페이스
- interface 지정자를 통해 선언
  - 추상 메서드 외 일반 필드나 메소드를 멤버로 가질 수 없음
  - 상수 필드는 포함 가능(public static final, 생략 시 자동 설정)
  - implements로 상속, 상속 시 모든 메소드 구현 필요


```java
interface AbstractInterface {
    String value;           // Error 발생, interface 내부에는 변수 선언 불가능

    void absMethod() {      // Error 발생, 메소드 내 Body 없어야 함
        System.out.println("Hello");
    }

    void absMethod2();
}
```