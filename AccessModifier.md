![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [public](#public)
- [protected](#protected)
- [default](#default)
- [private](#private)
- [예시](#예시)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 액세스 지정자(Access Modifier)

## public

- 접근 제한이 없음. 어디에서나 접근 가능


```java
public int nPublic;
```

## protected
- 같은 패키지 내에서 접근이 가능
- 다른 패키지에서도 상속을 받는 클래스 내부에서는 사용 가능(외부에서는 접근불가)


```java
protected int nProtected;
```


## default

- 같은 패키지 내에서만 접근이 가능
- 지정자가 없으면 이 값으로 기본 설정


```java
int nDefault;
```


## private

- 동일 클래스 내에서만 접근이 가능


```java
private int nPrivate;
```


## 예시

- AccessModifyOuter Package 내에 AccessModifierOutTest Class 생성


```java
package AccessModifyOuter;

public class AccessModifierOutTest {
    public int nPublic;
    protected int nProtected;
    int nDefault;
    private int nPrivate;
}
```


- AccessModify Package 내에 AccessModifierTest Class 생성


```java
package AccessModify;

import AccessModifyOuter.AccessModifierOutTest;

public class AccessModifierTest {
    public static void main(String[] args) {
        ChildExample ce = new ChildExample();
        ce.nDefault = 50;           // Error 발생, 다른 package에서는 접근 불가
        ce.nProtected = 60;         // Error 발생, 다른 package에서는 접근 불가
        ce.nPublic = 70;
        ce.nPrivate = 80;           // Error 발생, 다른 class에서는 접근 불가

        AccessModifierOutTest amot = new AccessModifierOutTest();
        ce.nDefault = 50;           // Error 발생, 다른 package에서는 접근 불가
        ce.nProtected = 60;         // Error 발생, 다른 package에서는 접근 불가
        ce.nPublic = 80;
        ce.nPrivate = 80;           // Error 발생, 다른 class에서는 접근 불가

        InnerAccessModifierTest iamt = new InnerAccessModifierTest();
        iamt.nDefault = 50;         // 동일 package 내 접근 가능
        iamt.nProtected = 60;       // 동일 package 내 접근 가능
        ce.nPublic = 80;
        iamt.nPrivate = 80;         // Error 발생, 다른 class에서는 접근 불가

    }
}

class ChildExample extends AccessModifierOutTest {

}

class InnerAccessModifierTest {
    public int nPublic;
    protected int nProtected;
    int nDefault;
    private int nPrivate;
}

```