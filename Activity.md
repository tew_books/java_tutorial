![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Activity](#activity)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
# Activity
## Activity

- main() 메서드를 사용하여 앱을 실행하는 프로그래밍 패러다임과 달리 Android 시스템은 수명 주기의 특정 단계에 해당하는 특정 콜백 메서드를 호출
![activity_lifecycle](https://user-images.githubusercontent.com/47145871/71087021-8a010380-21de-11ea-93e1-4df6da899b76.png)


- onCreate()
  - 시스템이 활동을 생성할 때 실행, 구현 시 활동의 필수 구성요소를 초기화.
  - 여기에서 뷰를 생성하고 데이터를 목록에 결합.
  - setContentView()를 호출하여 사용자 인터페이스를 위한 레이아웃을 정의해야 하며, 이 작업이 가장 중요<br/><br/>
- onStart()
  - onCreate() 종료 후 액티비티는 '시작됨' 상태로 전환되고 활동이 사용자에게 표시됨
  - 액티비티가 포그라운드로 나오기 전의 최종 준비에 준하는 작업이 포함  <br/><br/>
- onResume()
  - 액티비티가 사용자와 상호작용을 시작하기 직전 호출
  - 이 시점에서 액티비티는 스택의 맨 위에 있으며 모든 사용자 입력을 체크
  - 앱의 핵심 기능은 대부분 onResume() 메소드에서 구현  <br/><br/>
- onPause()
  - onPause()은 항상 onResume() 뒤에 위치
  - 액티비티가 포커스를 잃고 '일시중지됨' 상태로 전환될 때 시스템은 onPause()를 호출
  - 주로 사용자가 뒤로가기, 또는 최근 버튼을 탭할 때 발생
  - 시스템이 onPause()를 호출할 때 이는 엄밀히 말하면 액티비티는 여전히 부분적으로 표시되지만 대체로 사용자가 액티비티를 떠나고 있으며 액티비티가 조만간 '중지됨' 또는 '다시 시작됨' 상태로 전환됨을 나타냄  <br/><br/>
- onStop()
  - onPause() 이후 '일시중지됨' 상태로 전환되면 발생하는 상황에 따라 onStop() 또는 onResume()을 호출
  - 액티비티가 사용자에게 더 이상 표시되지 않을 때 시스템은 onStop()을 호출
  - 이는 액티비티가 제거 중이거나, 새 액티비티가 시작 중이거나, 기존 액티비티가 '다시 시작됨' 상태로 전환 중이고 중지된 액티비티를 다룰 때 발생
  - 이 모든 상황에서 중지된 액티비티는 더 이상 표시되지 않음  <br/><br/>


시스템이 호출하는 다음 콜백이 액티비티가 사용자와 상호작용하기 위해 다시 시작되면 onRestart()이며 완전히 종료되면 onDestroy()  

  
- onRestart()
  - '중지됨' 상태의 액티비티가 다시 시작되려고 할 때 시스템은 이 콜백을 호출
  - onRestart()는 액티비티가 중지된 시간부터 상태를 복원
  - 이 콜백 뒤에는 항상 onStart()가 옴  <br/><br/>
- onDestroy()
  - 시스템은 액티비티가 제거되기 전에 이 콜백을 호출
  - 일반적으로 액티비티의 모든 리소스를 해제하도록 구현  <br/><br/>
 
   
참고 : https://developer.android.com/guide/components/activities/activity-lifecycle.html