![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [배열](#배열)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 배열(Array)

## 배열
- 배열이란 타입형 변수, 혹은 나아가 객체들의 집합을 의미
  - 집합을 구성하는 요소들은 동일한 타입으로만 이루어 집니다.
  - 메모리에 여러 칸을 할당받아 한 줄로 차곡차곡 저장!
  - 구성 요소
    - 요소(element) : 배열을 구성하는 각각의 값
    - 인덱스(index) : 배열의 위치를 가리키는 번호
  - 배열의 크기는 처음 선언 이후 고정
  - 인덱스의 시작은 0입니다.(중요!)
- 배열 선언 및 할당


```java
public class ArrayTest {
    public static void main(String[] args) {
        int[] array;            //선언만 하는 경우
        int array1[];           //[]의 위치를 바꿔서 할 수 있습니다.
        
//        이 때 int[] array = {1,2,3,4}; 와 같은 방식으로 초기화를 해줄 수 있고
//        int[] array;
//        array = new int[] {1,2,3,4}; 와 같은 방식으로 초기화를 해줄 수 있습니다.
//        위의 방식은 주로 지역변수로 사용될 때 쓰이고, 아래의 방식은 주로 전역변수로 쓰일 때 쓰이게 됩니다.
        

        int array2[] = new int[4];
        String stArray[] = new String[3];

        int array3[] = null;

        for (int i = 0; i < 4; i++) {
            System.out.println(array2[i]);
        }

        System.out.println(array3);

        for (int i = 0; i < 3; i++) {
            System.out.println(stArray[i]);
        }
    }
}

```

- 결과값

```java
0
0
0
0
null
null
null
null

```

- ‘[ ]’ 로 배열 선언. Java의 경우 변수형, 변수명 어디 뒤에 붙이든 상관없음
- 배열 선언과 동시에 new 연산자를 이용해 사이즈 할당.  
이 때 변수형에 따라 0 or null 값으로 자동 초기화됨. (boolean은 false로)

- 배열 초기화 및 크기 확인

```java
        int array[] = new int[] {1,2,3,4,5};    //배열 초기화 원형
//        int array2[] = new int[5] {1,2,3,4,5};  //직접 초기화 시에는 추가적인 크기 지정 불가
        int array3[] = {2,3,4,5,6,7,8};         //배열 초기화 축약형
        int array4[];
        array4 = new int[] {3,4,5,6,7};         //배열 선언 후 할당 초기화

        int arrayLength = array3.length;        //선언한 배열의 크기

        System.out.println(array3[1]);
        array3[1] = 4;                          //배열값 변경
        System.out.println(array3[1]);
        
```
