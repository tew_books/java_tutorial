![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [클래스](#클래스)
- [생성자(Constructor)](#생성자constructor)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 클래스(Class)
## 클래스

- 앞서 언급된 것과 같이 클래스란 같은 집단에 속하는 속성(attribute)과 행위(behavior)를 정의한 것
- 액세스 지정자에 따라 다른 클래스에서 객체 호출 가능 (private은 안되죠..)
- 클래스는 참조형이므로 반드시 new 연산자로 할당
  - 객체 생성 후 객체를 가리킬 수 있는 클래스 내 타입형 변수에 값을 대입
  - 객체의 필드나 메소드 접근 시에는 ‘ . ’ 연산자 이용.
  - 클래스 내 변수 선언과 동시에 초기화도 가능 (default 값 선언)
  - 클래스 내 변수는 캡슐화를 위해 private으로 선언  
    getter & setter 로 조회만 가능한 메소드를 사용


- 예시


```java
class Computer {
    String color;               // default 값 설정 가능
    String cpu;
    int ram;
    int price;
    boolean portable;

    void webSurfing() {
        if (cpu.equals("Intel")) {
            System.out.println("Internet Explorer");
        } else {
            System.out.println("Safari");
        }
    }

    void stop() {
        System.out.println("시스템을 종료합니다.");
    }
}
```


```java
class Computer2 {
    private String color;
    private String cpu;
    private int ram;
    private int price;
    private boolean portable;
```


```java
public String getColor() {
    return color;
}

public void setColor(String color) {
    this.color = color;
}
```


```jave
Computer2 desktop2 = new Computer2();
desktop2.setColor("Blue");
desktop2.color = "Blue";        // private이므로 접근 불가
```



```java
public class ClassTest {
    public static void main(String[] args) {
        Computer laptop = new Computer();
        Computer desktop = new Computer();

        System.out.println(laptop.color);       //  값 선언 전 color 체크 = null

        laptop.color = "Silver";
        laptop.cpu = "Intel";
        laptop.ram = 16;
        laptop.price = 2000000;
        laptop.portable = true;

        System.out.println(laptop.color);       //  값 선언 후 color 체크 = Silver

        laptop.webSurfing();                    // os = true 인 경우 Internet Explorer
        desktop.cpu = "MAC";
        desktop.webSurfing();                   // 설정 안하는 경우 기본 os = false 이므로 Safari

        laptop.stop();
        desktop.stop();
    }
}
```

## 생성자(Constructor)
- 생성자란 인스턴스를 생성해주는 역할을 하는 일종의 메소드.  
따라서 값은 반환하지 않는다.
- 생성자의 이름은 클래스의 이름과 동일하게 해야 함


```java
public Computer() {     // 생성자도 오버로딩 가능
}

public Computer(String color, String cpu, int ram, int price, boolean portable) {
    this.color = color;     // this를 통해 해당 클래스의 변수를 지정
    this.cpu = cpu;
    this.ram = ram;
    this.price = price;
    this.portable = portable;
}
```


```java
Computer laptop2 = new Computer("Silver", "Intel", 16, 200000, true);       //생성자 선언을 통한 초기화

...

laptop.color = "Silver";
laptop.cpu = "Intel";
laptop.ram = 16;
laptop.price = 2000000;
laptop.portable = true;

...
```
