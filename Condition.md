![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [if문](#if-문)
- [switch/case문](#switchcase-문)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 조건문(Condition)
## if 문
- if, else if, else로 구성
  - 기본 형태(if문) : *if(조건식) <수행할 코드>*  
  : 조건식이 true인 경우 코드를 수행
    - 수행할 코드가 2줄 이상인 경우  
     { } (중괄호) 필요 (1줄에도 써주면 이후 수정에서 편함)
  -	else if문  
  *if (조건식) { <수행할 코드1> } else { <수행할 코드2> }*  
  : 조건식이 true 인 경우 코드1 수행, false인 경우 코드2 수행
  - if else문  
  *if (조건식1) { <코드1> } else if(조건식2) { <코드2> } else { <코드3> }*  
  : 다중 조건이 주어졌을 때 사용 ( if() { } else { if( ) { } else { } } 의 무한 반복 막기 )
 
 ```java
 if (i < 3) {
    // 입력받은 i가 3보다 작은 경우
} else if (i < 5) {
    // 입력받은 i가 3보다는 크지만 5보다는 작은 경우
} else if (i < 7) {
    // 입력받은 i가 3보다 크고, 5보다 크지만 7보다 작은 경우
} else {
    // 입력받은 i가 7보다 크거나 같은 경우
}
```
 
## switch/case 문
- switch, case 로 구성
  -	기본 형태 : *switch(조건) : { case 조건 : 코드 break; default: break;}*  
  : 조건을 충족하는 case에 맞는 코드 수행

 ```java
switch (i) {
    case 0:
        // i == 0 인 경우 수행
        break; // break를 통해 조건이 충족되면 빠져나감.
    case 1:
        // i == 1 인 경우 수행
          break;   // 이처럼 break를 넣어주지 않으면 i == 1 인 경우를 수행 후 i == 2 도 경우도 연달아 수행됨
    case 2:
        // i == 2 인 경우 수행
        break;
    default:
        // i가 어느 값에도 해당되지 않는 경우 기본적으로 수행될 부분 (생략 가능)
        break;
}
```
