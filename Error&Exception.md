![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [기본 정보](#기본-정보)
- [예외 처리(Error Handling)](#예외-처리error-handling)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Error & Exception

## 기본 정보

![Exception](https://user-images.githubusercontent.com/47145871/69940631-14cad880-1526-11ea-9ccd-69b67d6b61c4.png)

- Error : 프로그램 외적 요인에 의해 프로그램 내부적으로 복구가 불가능한 심각한 오류.(장치결함 또는 메모리 부족 등)
- Exception : 프로그램 내부적으로 오류 발생 조건만 해결된다면 복구가 가능한 경미한 오류.
- Checked Exception : 문법적으로 예외 처리가 강제된 예외로, 프로그램 외적 요인에 의해 발생 가능한 예외.(프로세스 충돌, 사용자의 오 입력 등)
- Unchecked Exception : 예외 처리를 생략 가능한 예외로, 프로그램 자체의 논리적 오류에 의해 발생 가능한 예외.(개발자의 단순 실수 등)

  - Checked Exception과 Unchecked Exception의 경우  
    Checked Exception은 반드시 예외 처리가 필요하지만  
    Unchecked Exception의 경우 예외 처리 없이도 컴파일이 가능.  
    (물론 예외 발생으로 인한 강제 종료를 보게 될 것입니다.)
  - 즉, Checked Exception은 컴파일 단계에서 확인하지만 Unchecked Exception은 실행 단계에서 확인!


## 예외 처리(Error Handling)
  - try-catch-finally
    - try { 블럭 안에 예외가 발생할 우려가 있는 코드를 입력 }
    - catch (throw할 Exception 선언) { catch 블록 안에 에러 혹은 예외 발생 시 수행할 코드를 입력 }  
      throw할 에러가 여러개인 경우 catch 블럭을 여러개 생성하거나 |(파이프라인) 연산자를 통해 Exception 선별 가능 (InterruptExcpetion | ArithmeticException e)
    - finally { 예외 여부와는 관계없이 무조건 수행할 코드를 입력 }  
    주로 자원 해제(scanner.close()) 와 같은 용도로 많이 쓰임


```java
public class ExceptionHandlingTest {
    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("HH-mm-ssSSS");
        String stTime = sdf.format(new Date(time));
        System.out.println(stTime + " // 0초");
        
        try {                                      // 예외가 발생할 수 있는 코드
            Thread.sleep(1000);                    // 기초적인 Thread 사용 법 (1000milli second = 1초 동안 작업 수행 미룸)

            long newTime = System.currentTimeMillis();
            String stNewTime = sdf.format(new Date(newTime));
            System.out.println(stNewTime + " // 1초가 지났습니다.");

        } catch (InterruptedException e) {         // 예외 발생시 처리될 코드
            e.printStackTrace();
        } finally {                                // 예외가 발생 여부와는 별개로 무조건 실행되야하는 코드
            long newTime = System.currentTimeMillis();
            String stNewTime = sdf.format(new Date(newTime));
            System.out.println(stNewTime + " // 1초가 지났습니다.");
        }
                                                    // 각 처리 부분에 return을 넣는다면? 어떻게 될까?
    }
}
```

  - try, catch 내 return시 finally 구문을 거쳐 종료. finally 내 return 시 try 구문에서 발생한 Exception이 출력되지 않는다.
  - **NullPointerException**은 앞으로 여러분이 가장 많이 보게 될 Exception!!

  - 결과
 
  
```xml
10-33-39353 // 0초                          // try 전의 현재 시간 // 0초 출력
10-33-40382 // 1초가 지났습니다.            // try 블럭 내의 1초 후 시간 출력
10-33-40382 // 1초가 지났습니다.            // finally 블럭 내의 1초 후 시간 출력 (동일)
```

  - try처리를 하지 않는 경우


```java
Thread.sleep(1000);             // sleep에 빨간 에러 줄 발생
```


```xml
java: unreported exception java.lang.InterruptedException; must be caught or declared to be thrown
```

와 같이 InterruptedException 발생 (컴파일 불가)


  - 사용자 정의 예외 : 사용자가 예외 규정을 임의로 만들어서 호출 가능


```java
class UserDefException extends Exception{
    UserDefException(String exContent){
        super(exContent);
    }
}

...
if(!isRightCondition){
    throw new UserDefException("Unacceptable Rule!!!")
}
...
```