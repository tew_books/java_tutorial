![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Gradle](#Gradle)
- [build.gradle-Project](#buildgradle-project)
- [build.gradle-Module](#buildgradle-module)
- [Dependency](#dependecies)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Gradle
## Gradle
- Gradle은 Groovy를 이용한 빌드 자동화 시스템
- Groovy와 유사한 도메인 언어를 채용하였으며, 현재 안드로이드 앱을 만드는데 필요한 안드로이드 스튜디오의 공식 빌드 시스템


## build.gradle-Project


```xml
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
        
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.5.3'
        
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```



## build.gradle-Module


```xml
apply plugin: 'com.android.application'
//apply plugin: 'com.android.library'    //Library 빌드를 할 때는 해당 플러그인으로 변경

android {
    compileSdkVersion 29
    buildToolsVersion "29.0.2"
    defaultConfig {
        applicationId "team.everywhere.androidweekdayproject"
        minSdkVersion 25
        targetSdkVersion 29
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'androidx.test.ext:junit:1.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
}
```


- application 제작을 위해 apply plugin은 'com.android.application'으로 사용 --> *.apk (android package).  
  library로 빌드를 할 때는 'com.android.library'로 사용 --> *.aar (android archive)

- android 내에 application의 기본 속성을 지정
  - compileSdkVersion : 앱 컴파일시 사용할 SDK 버전
  - buildToolsVersion : 사용할 빌드툴의 버전
  - defaultConfig{...} : 기본값 설정. Manifest에서 사용하는 설정들에 대한 동적인 옵션을 설정해줌
    - applicationId : 패키지 이름으로 사용함. 구글 플레이 스토어에 동일한 이름이 있으면 안됨. 마켓에 등록하고 나면 변경 불가(새로 등록해야 함)
    - minSdkVersion : 지원하는 최소 SDK 버전. SDK 버전이 해당 값보다 낮으면 스토어에서 검색이 안됨(지원불가 기기)
    - targetSdkVersion : 어플리케이션이 타겟으로 삼는 SDK 버전. 해당 SDK 버전을 기준으로 만들었다고 할 수 있음. 최신버전보다 낮을 경우 안드로이드 스튜디오에서 경고 표시함
    - versionCode : 어플리케이션의 버전을 명시. 플레이스토어에 업데이트를 할 경우 기존 업로드된 apk보다 높은 값을 입력해야 함
    - versionName : 버전의 이름을 명시. 보통 1.0.0, 1.1.3 과 같이 3자리로 많이 구성 (정해진 것은 없습니다.)
    - testInstrumentationRunner : 테스트 환경 구성
  - buildTypes : debug, release 구분을 통해 각 type build 시에 따른 개별 설정
    - minifyEnabled : 코드 경량화(축소). 코드 축소를 통해 어플리케이션의 부하를 줄임.  
    앱에서 사용하지 않는 클래스, 필드, 메서드, 속성 및 라이브러리 종속성을 감지하여 안전하게 삭제  
    (앱 및 앱이 참조하는 라이브러리에서 메소드가 65,536(64k)를 초과하면 빌드 아키텍처의 제한에 도달하여 빌드 오류 발생)
    - proguardFiles 코드의 난독화.


## dependecies
- dependencies : 외부 라이브러리의 의존성을 추가
  - implementation 명령어를 통해 추가 (기존에는 compile로 추가하였으나, deprecate 될 예정)
  - 라이브러리 의존성 추가는 주로 maven repository를 통해 하게 됨
  - 두 가지 방법이 존재 (최근에는 위의 방식을 쓰는 추세)  
    implementation 'androidx.appcompat:appcompat:1.1.0'  
    implementation group: 'androidx.appcompat', name:'appcompat', version:'1.1.0'
  - testImplementation 는 테스트를 위해 사용