![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [상속](#상속)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 상속(Inheritance)
## 상속

- 객체 지향 프로그래밍의 특성 중 상속성.
- 어떤 객체에 생성된 변수와 메서드를 다른 객체가 물려 받는 것을 의미
- 사용하는 경우 
  - 코드를 변경할 수 없는 경우 (타인이 객체를 생성)
  - 객체가 다른 곳에서도 사용돼서 메소드 추가 시 다른 곳에도 불필요한 기능이 포함될 우려가 있음.
- 상속을 하는 객체와 받는 객체는 super – sub 혹은 부모-자식 으로 부름

    
```java
class Animal{
    String name;
    String gender;
    int age;
    int weight;

    void eat(){
        System.out.println("EAT!!");
    }

    void sleep(){
        System.out.println("zzzzZZZZZzzzzZZZZ");
    }
}

class Human extends Animal{
    int height;
    String language;

    void talk() {
        System.out.println("I'm talking to you!");
    }

    void work() {
        System.out.println("Show me the money");
    }
}

class Dog extends Animal{
    int ageComparedHuman;
    String species;

    void bark() {
        System.out.println("Meong! Meong!");
    }

    void shakeTail(){
        System.out.println("Tail Shaking");
    }
}

public class InheritanceTest {
    public static void main(String[] args) {
        Human human1 = new Human();
        human1.eat();
        human1.sleep();
        human1.talk();
        human1.work();
//        human1.bark();              // 상속받지 않은 메소드
//        human1.shakeTail();         // 및 변수는 사용 불가
    }
}
```

- Java는 다중상속이 불가능?
  - 상속-상속-상속-상속-… -> 다중상속 X

```java
class Human extends Animal{
    int height;
    String language;

    void talk() {
        System.out.println("I'm talking to you!");
    }

    void work() {
        System.out.println("Show me the money");
    }
}

class Asian extends Human{
    String whichPartInAsia;
}
```
  
  - 상속+상속+상속+상속+… -> 다중상속 O


```java
class Human extends Animal, Dog{
    // 이 때는 Class cannot extend multiple classes 에러 발생
}
```

- 대안으로 *implements*를 통한 인터페이스를 구현
  - 상속과 유사해보이지만 상속과는 다르게 인터페이스만 얻어 옴 (인터페이스에 대해서는 다음 챕터에서 설명)