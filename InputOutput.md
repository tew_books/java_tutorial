![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [출력](#출력)
- [입력](#입력)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 출력과 입력
## 출력

```java
System.out.println("Internet Explorer");
System.out.printf("%s", "이름");
System.out.printf("%d", 3);
System.out.printf("%b", true);
```

- System.out.print() & System.out.println() & System.out.printf() 가 대표적.  
  System.out.printf()의 경우 괄호 안에 format을 정해주는 방식이며 이 때, format으로는  
  %s = String  
  %d = int  
  %b = boolean  
  %f = float, double  
  를 주로 사용하게 됨


## 입력
1.Scanner 사용

```java
Scanner scanner = new Scanner(System.in);
System.out.print("이름을 입력하시오 : ");
String name = scanner.next();
System.out.println("이름은: " + name + "입니다.");
scanner.close();
```

- Scanner는 사용이 완전히 끝나면 반드시 close() 호출 필요 (메모리 leak 방지);

2.JOptionPane 사용

```java
String str1 = JOptionPane.showInputDialog("이름 입력 : ");
String str2 = JOptionPane.showInputDialog("이름 입력 : ");

System.out.println("이름은 " + str1 + "이고, 나이는 " + str2 + "살 입니다.");
```

컴파일 시 팝업 창이 뜨는 것을 확인할 수 있다.

