![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Layout](#layout)
- [LinearLayout](#linearlayout)
- [RelativeLayout](#relativelayout)
- [ConstraintLayout](#constraintlayout)
- [GridLayout](#gridlayout)
- [FrameLayout](#framelayout)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
# Layout
## Layout

- Layout : View 위젯들을 그룹화하여 배치하기 위한 용도로 사용되는 ViewGroup
  - 자체적인 UI 표시 기능이나 사용자 이벤트 처리 기능은 매우 제한적
  - 종류는 많지만 자주 사용되는 Layout은 LinearLayout, RelativeLayout, ConstraintLayout, GridLayout, FrameLayout 정도
  - Layout을 기준으로 왼쪽은 Start, 오른쪽은 End, 위는 Top, 아래는 Bottom으로 지칭
  

## LinearLayout
- LinearLayout : Linear (선형) 라는 단어 그대로 View 위젯들을 가로, 세로 단 방향으로만 나열
  - orientation, weightSum 을 통해 나열 설정
  - child는 weight 를 통해서 비율 설정


```xml
<LinearLayout
    android:id="@+id/linearLayout"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal or vertical"
    android:weightSum="10">
    ...

    <TextView
    ...
    android:weight="3"
    .../>

    ...
    </LinearLayout>
```


## RelativeLayout
- RelativeLayout : Relative (상대적) 라는 단어 그대로 View 위젯 간 위치를 상대적으로 나열
  - start, end, above, below 를 통해 위치 설정


```xml
<RelativeLayout
    android:layout_width="300dp"
    android:layout_height="500dp"
    app:layout_constraintBottom_toBottomOf="parent"
    app:layout_constraintEnd_toEndOf="parent"
    app:layout_constraintStart_toStartOf="parent">
    
    ...
    
    <TextView
        android:id="@+id/txtBase"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="140dp"
        android:layout_marginTop="190dp"
        android:text="Hello" />
        
    <TextView
        android:id="@+id/txtEnd"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@+id/txtBase"
        android:layout_marginStart="40dp"
        android:layout_toEndOf="@+id/txtBase"
        android:text="End World!" />
    
    <TextView
        android:id="@+id/txtStart"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@+id/txtBase"
        android:layout_marginStart="40dp"
        android:text="Start World!" />
    
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_above="@+id/txtEnd"
        android:layout_marginBottom="30dp"
        android:layout_toEndOf="@+id/txtStart"
        android:text="Above World!" />
    
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@+id/txtBase"
        android:layout_marginTop="30dp"
        android:layout_toEndOf="@+id/txtStart"
        android:text="Below World!"
        />
        
    ...
    
</RelativeLayout>
```


![image](https://user-images.githubusercontent.com/45971970/71316422-a2a93c00-24b2-11ea-8e33-e3d48360bf84.png)




## ConstraintLayout
- ConstraintLayout : Linear, Relative 에 이어 나온 2016년에 나온 Layout
  - 최근의 개발은 ConstraintLayout으로 진행
  - 다양한 비율과 해상도까지 지원하기 위해 생성
  - ConstraintLayout이 없으면 Linear - Linear - Relative - Relative 등 의 반복으로 생성해야 함
  - margin, layout_constraint 속성을 이용 하여 View를 연결
  - app:layout_constraintDimensionRatio="" 속성을 이용한 고정 비율 설정 가능
  - 수평(Start, End), 수직(Top, Bottom) 속성 중 각각 하나씩은 constraint 처리해주어야 함(하지 않는 경우 Start, Top 이 각각 0,0 으로 자동 constraint 됨)
  - width 혹은 height 값을 match_parent로 설정하는 경우 해당 값에 맞는 속성(수평, 수직) 값은 constraint 처리하지 않아도 됨 (constraint하지 않아도 화면을 꽉 채우므로)
  


```xml
    <View
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintStart_toStartOf="@+id/viewName"
        app:layout_constraintStart_toEndOf="@+id/viewName"
        app:layout_constraintEnd_toEndOf="@+id/viewName"
        app:layout_constraintEnd_toStartOf="@+id/viewName"
        app:layout_constraintTop_toTopOf="@+id/viewName"
        app:layout_constraintTop_toBottomOf="@+id/viewName"
        app:layout_constraintBottom_toBottomOf="@+id/viewName"
        app:layout_constraintBottom_toTopOf="@+id/viewName"
        app:layout_constraintBaseline_toBaselineOf="@+id/viewName"/>
```


```xml
    layout_constraintStart_toStartOf="@+id/viewName"        // viewName이라는 id를 갖는 View의 수평 시작점이 해당 뷰의 수평 시작점으로 설정
    layout_constraintStart_toEndOf="@+id/viewName"          // viewName의 수평 종료점(오른쪽)에 현재 생성하는 View의 수평 시작점을 연결  
    layout_constraintEnd_toEndOf="@+id/viewName"            // viewName의 종료점을 해당 뷰의 종료점으로 설정
    layout_constraintEnd_toStartOf="@+id/viewName"          // viewName의 시작점이 해당 뷰의 종료점
    layout_constraintTop_toTopOf="@+id/viewName"            // viewName의 수직 시작점이 해당 뷰의 수직 시작점
    layout_constraintTop_toBottomOf="@+id/viewName"         // viewName의 시작점과 같은 시작점에 연결
    layout_constraintBottom_toBottomOf="@+id/viewName"      // viewName의 시작점과 같은 시작점에 연결
    layout_constraintBottom_toTopOf="@+id/viewName"         // viewName의 시작점과 같은 시작점에 연결
    layout_constraintBaseline_toBaselineOf="@+id/viewName"  // viewName의 시작점과 같은 시작점에 연결
```


  - 예시
![image](https://user-images.githubusercontent.com/45971970/71316612-bace8a80-24b5-11ea-8d57-d6e7b30c0639.png)


```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">


    <TextView
        android:id="@+id/viewName"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="기준"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="왼쪽"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintEnd_toStartOf="@+id/viewName"
        app:layout_constraintTop_toTopOf="@+id/viewName"
        app:layout_constraintBottom_toBottomOf="@+id/viewName"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="오른쪽"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintStart_toEndOf="@+id/viewName"
        app:layout_constraintTop_toTopOf="@+id/viewName"
        app:layout_constraintBottom_toBottomOf="@+id/viewName"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="위"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintBottom_toTopOf="@+id/viewName"
        app:layout_constraintStart_toStartOf="@+id/viewName"
        app:layout_constraintEnd_toEndOf="@+id/viewName"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="아래"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintTop_toBottomOf="@+id/viewName"
        app:layout_constraintStart_toStartOf="@+id/viewName"
        app:layout_constraintEnd_toEndOf="@+id/viewName"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="좌측상단"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintEnd_toStartOf="@+id/viewName"
        app:layout_constraintBottom_toTopOf="@+id/viewName"/>
    
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="우측상단"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintStart_toEndOf="@+id/viewName"
        app:layout_constraintBottom_toTopOf="@+id/viewName" />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="좌측하단"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintEnd_toStartOf="@+id/viewName"
        app:layout_constraintTop_toBottomOf="@+id/viewName"/>

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="우측하단"
        android:textColor="#000000"
        android:textSize="18sp"
        app:layout_constraintStart_toEndOf="@+id/viewName"
        app:layout_constraintTop_toBottomOf="@+id/viewName"/>

    
</androidx.constraintlayout.widget.ConstraintLayout>
```





```xml
<androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/hello_world"
        ...
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/et" />

    <EditText
        android:id="@+id/et"
        ...
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
        
</androidx.constraintlayout.widget.ConstraintLayout>

```


## GridLayout
 - GridLayout : 동일한 내용의 View를 반복하여 표현하는 경우 주로 사용. GridLayout 자체를 생성하여 사용하기 보다는, Adapter를 통해 LayoutStyle을 가져오는데 주로 사용
 - row, column 으로 값을 설정



## FrameLayout
 - FrameLayout : 하나의 자식 View만 표시할 때 사용
   - 주로 Fragment 구현을 위해 쓰임(Fragment의 container로 사용)
   - 순서에 따라 디스플레이에 구현되기 때문에 배치를 잘해주어야 함

