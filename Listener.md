---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Callback](#callback)
- [Listener](#listener)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Listener
## Callback
- 특정 이벤트가 발생시 시스템에 의해서 자동으로 호출되는 메서드  
  ex) boolean onTouchEvent(MotionEvent event), boolean onKeyDown(int keyCode, KeyEvent event), boolean onKeyUp(int keyCode, KeyEvent event)
- View 클래스로부터 상속받은 뒤 오버라이딩(재정의) 하여 이벤트 발생 시의 처리할 액션을 정의
- Callback을 사용하기 위해서는 항상 super클래스를 상속받아 재정의 해야 함


## Listener
- Interface  
- 
  - 앞서 Abstract(추상)에서 배운 인터페이스와 동일
  - 인터페이스는 implements 시 클래스에서 반드시 구현해야할 행동을 지정해 주는 추상 타입
  - implements 를 통해 구현할 때 인터페이스 내 정의된 메소드는 반드시 구현해야 함

- Listener : 사용자가 어떤 액션을 할 때 마다 즉, 이벤트 발생 시 리스너를 선언한 곳에 메세지를 전달하여 미리 약속된 이벤트를 발생시킴 (이벤트 핸들러)  
- 
  - 안드로이드 개발을 하면서 오타가 굉장히 많은 부분 (feat. Listenr, Listner...)
  - Callback보다 좀 더 범용적이고 간편한 방법으로 나온 것이 리스너(Listener)
  - implements를 통해 메소드를 Override 하는 방법과 View에 직접 선언해주는 방법이 있음
  - View에 직접 선언해주는 경우 setOnXXXXXXXXXXListener(new 해당Listener {} );  호출 후 그에 알맞는 메소드 Overriding으로 각 이벤트에 알맞는 처리 필요  
    코드 재활용 성 및 가독성, 유지보수가 어려워짐
  - implements하는 경우 setOnXXXXXXXXXXListener(this); 로 처리해 준 다음 switch / case 구문을 통해 view.getId 혹은 view.equal 값으로 각 listener에 대한 이벤트를 처리해 줌
  - 안드로이드에서 대표적으로 많이 쓰이는 Listener는 OnClickListener
  - OnClick의 경우 xml에서도 연결 가능
  - vs Callback? : Callback은 어떤 작업이 종료되는 시점에 되호출, Listener는 해당 Listener가 등록된 위치의 Callback들을 호출하는 역할 (둘의 동작 원리는 같다)



```java
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ...
    
    @Override
    public void OnCreate(Bundle savedInstanceState){
        ...
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //클릭 시 수행될 이벤트 작성
            }
        });
        
        btn.setOnClickListener(this)
    }
    
    @Override
    public void onClick(View v) {
        if(v.equals(btn)){
            
        }
        
        // 혹은
        
        switch (v.getId()){
            case R.id.btn:
                // 수행될 이벤트 작성
                break;
        }
    }
    
}
```


