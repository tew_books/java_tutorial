![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [for문](#for-문)
- [while문](#while-문)
- [do-while문](#do-while-문)
- [for each문](#for-each-문)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 반복문(Loop)
- 특정 조건을 설정하고, 해당 조건을 만족시킬 때까지 명령을 계속 반복하는 코드  

## for 문
- 기본 형태:  for(int i = 0; i < (조건); i++){ <수행할 코드> }
  - 조건을 만족시키는 경우 (true일 때) 코드를 수행하고, i++를 수행한다. 

## while 문
- 기본 형태: while (조건) { <수행할 코드> };
  - 조건의 횟수가 정해지지 않은 경우 주로 사용
  

## do-while 문
- 기본 형태: do{ <수행할 코드> } while (조건);
  - while문과는 다르게 우선 처리를 한 다음 조건식을 확인 


## for each 문
- 주로 배열과 함께 쓰임
- 기본 형태: for(type 변수 : 배열(혹은 객체) ) { <수행할 코드> }  
  반복횟수를 명시적으로 줄 수 없음(break는 가능)

```java
/**
 * 일반적인 for Loop
 */
for (int i = 0; i < 5; i++) {
    // i 가 5와 같아질 때까지 i++를 수행
    System.out.print(i + " ");
}
System.out.println();
/**
 * do-while Loop
 */
int i = 0;
do {
    System.out.print(i + " ");
    i++;
} while (i < 5); // i 가 5와 같아질 때까지 i++를 수행
System.out.println();
/**
 * for each Loop
 */
int[] array = new int[]{1, 2, 3, 4, 5};
for (int j : array) { // array 혹은 객체에서 순차적으로 j에 대입되어 for문을 수행
    System.out.print(j + " ");
}
```
