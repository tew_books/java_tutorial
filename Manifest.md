![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Manifest](#manifest)
- [Application](#application)
- [Activity](#activity)
- [Service](#service)
- [BroadcastReceiver](#broadcastreceiver)
- [ContentsProvider](#contentsprovider)
- [Permission](#permission)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Manifest
## Manifest
- Manifest?
  - Manifest 파일은 Android 빌드 도구, Android 운영체제 및 Google Play에 앱에 관한 필수 정보를 설명 (고유한 값이므로 build.gradle의 applicationId와 동일하게 유지하는 편이 좋음)
  - 앱의 구성 요소(Activity, Service, Broadcast Receiver, Contents Provider) 선언
  - 시스템 또는 다른 앱의 보호된 부분에 액세스하기 위해 필요한 권한(Permission) 선언
  - 기기 호환성(uses-feature) 선언


```xml
<manifest> 
앱의 패키지 이름(코드의 네임스페이스와 일치)
 package=”com.example.packagename”
package 특성은 빌드 도구에서 
     1. R.java 클래스의 네임스페이스로 사용됨
	 2. Manifest에 선언되어 있는 상대 클래스 이름을 확인
	    (ex: <activity android:name=”.MainActivity”> = 
            <activity android:name= “com.example.packagename.MainActivity”> )
            

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="team.everywhere.androidproject">
```


## Application
- Application의 메인 특성
  - 해당 xml에서 요소를 선언하지 않고 하위 클래스로 지정하려고 하면 시스템에서 이를 시작할 수 없음.


```xml
<application>
```


```xml
android:allowBackup="true"
android:icon="@mipmap/ic_launcher"
android:label="@string/app_name"
android:roundIcon="@mipmap/ic_launcher_round"
android:supportsRtl="true"
android:theme="@style/AppTheme"
```



와 같은 세팅이 앱 초기 생성 시 자동으로 설정됨. 
  - allowBackup – 세팅하지 않는 경우 자동으로 true 설정. 앱 삭제 후 재설치 할 때 구글 클라우드에 자동으로 데이터가 백업되어 자료를 자동으로 복원해줌
  -	icon – 어플리케이션의 아이콘 설정
  - label – 어플리케이션의 이름 설정
  -	roundIcon – 안드로이드 8.0부터 생긴 adaptive 아이콘의 일부. iOS와는 다르게 안드로이드는 아이콘을 개인의 취향에 맞춰 설정 가능하기 때문에 원형 icon을 설정할 때는 해당 코드에 있는 icon을 생성해줌.
  -	supportsRtl – 안드로이드 sdk버전이 17이상인 경우 Right To Left (이슬람 등에서 쓰는) 지원
  -	theme – 어플을 구성하는 뷰의 기본 테마를 설정. 
  

## Activity	
- Activity의 각 하위 클래스.


```xml
<activity> 
```


  - Activity는 화면의 UI를 담당하는 컴포넌트
  -	사용을 위해서는 Activity 클래스를 상속해야 한다(최근엔 AppCompatActivity를 상속
  -	안드로이드 어플리케이션은 반드시 하나 이상의 Activity가 있어야 한다
  -	Activity는 동시에 보여질 수 없음. (Fragment를 사용하게 됨)
  -	다른 어플리케이션에 있는 Activity를 불러낼 수 있음.
  -	Manifests 상 하위 클래스 선언 방법


```xml
<activity android:name=".MainActivity">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />

        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>
</activity>
```


  -	name – implementing할 activity의 이름
  -	intent : 컴포넌트 간 통신을 하기 위한 매세지 객체 (이후 Activity와 관련해서 주로 다룰 예정), 명시적 인텐트(Explicit Intent)와 암시적 인텐트(Implicit Intent)가 있음.
    - 명시적 인텐트 – 실행 혹은 호출할 대상이 명확한 경우 사용
    - 암시적 인텐트 – 요청한 정보를 처리할 수 있는 적절한 컴포넌트를 찾아서 사용자에게 대상과 처리 결과를 보여줌. (인터넷 주소 url 클릭 시 페이지를 띄울 어플 선택)


```xml
<intent-filter> 암시적 인텐트를 수신하기 위해 메니페스트에 선언해 줌.
```


- 각 인텐트 필터는 인텐트의 action, data, category를 근거로 어느 유형의 인텐트를 허용하는지 나타냄.
  - action : name속성에서 허용된 인텐트 작업을 선언. 여기서 . 앞에는 패키지가 생략된 것
  - data : 허용된 데이터 유형 선언. URI, 혹은 MIME 유형을 나타냄.
  - category : name 속성에서 허용된 인텐트 카테고리 선언. 
 - 위의 예시는 해당 intent가 MAIN으로 쓰일 작업이고, 런쳐 카테고리로 설정된 것이기 때문에, 어플을 실행했을 때 첫 화면으로 해당 intent filter가 속한 activity를 실행할 것.


```xml
<intent-filter>
    <action android:name="android.intent.action.SEND"/>
    <category android:name="android.intent.category.DEFAULT"/>
    <data android:mimeType="text/plain"/>
</intent-filter>
```


위와 같은 인텐트 필터는 mimeType이 text인 data 유형일 때 ACTION_SEND 인텐트를 수신 

  - mimeType ? Multipurpose Internet Mail Extensions  
    파일 변환, 혹은 파일의 형태를 의미. 초기에 이메일에 동봉할 파일을 텍스트 문자로 전환해서 전달하는데 사용됨.
  - 참고 : https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types


## Service
 - Service의 각 하위 클래스.


```xml
<service> 
```


  -	서비스는 백그라운드에서 실행되는 프로세스. 화면을 갖지 않는다
  - Activity와 마찬가지로 사용을 위해 매니페스트 xml에 등록해주어야 하며, Service를 상속함.
  - Activity 내에서 startService() 메소드를 통해 실행
  - 어플리케이션이 종료되도 계속해서 백그라운드에서 돌아감 (쓰레드)
  - 서비스 내 네트워크 연동을 통해 데이터 수신 가능


## BroadcastReceiver
- BroadcastReceiver의 각 하위 클래스.


```xml
<receiver> 
```

  -	브로드캐스팅 = 메시지를 객체에 전달하는 방법
  - 브로드캐스팅 리시버 = 실행 중 이벤트 발생 혹은 정보 전달, 즉 메시지 에 반응
    - registerReceiver() 메소드를 통해 등록
    - 서비스처럼 UI가 거의 없음


## ContentsProvider
- ContentProvider의 각 하위 클래스.


```xml
<provider> 
```


  - 데이터를 관리 및 다른 어플리케이션 데이터를 제공
  - 주로 파일 입출력, SQLite 등을 이용해서 데이터 관리
  - 데이터 전달을 위해 존재

참고 : https://developer.android.com/guide/topics/manifest/manifest-intro?hl=ko#reference

 
 
## Permission
 - 안드로이드 M(마시멜로, 6.0) OS 이후 사용하기 시작.
 - 6.0 이전 버전의 경우 앱 설치 전 모든 권한 요구, 거절 시 설치 불가
 - 6.0 이후 기본 권한만 설치 전에 물어보고, 위험 권한(Dangerous Permission)은 설치 후 사용자의 선택을 물어보게 됨
 - 기본 구조 : Manifests에 명시
 - 명시하지 않는 경우 requestPermission 을 수행하지 않음


```xml
<uses-permission android:name=”android.permission.@@@@@@@@@@@@@@” />
```


  - 실제 사용 예시


```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="android.permission.READ_PROFILE" />
    <uses-permission android:name="android.permission.READ_CONTACTS" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.REQUEST_INSTALL_PACKAGES" />
    <uses-permission android:name="android.permission.REQUEST_DELETE_PACKAGES" />
```


  https://developer.android.com/reference/android/Manifest.permission.html?hl=ko
    - 기본 플랫폼에서 정의한 권한 목록 : 157개의 퍼미션 존재


 - 특정 기능에 대해 필터링을 비활성화하려는 경우 android:required="false" 특성을 <uses-feature> 선언에 추가 



```xml
<uses-feature android:name="android.hardware.bluetooth_le" android:required="true" />
 ```


참고 : https://developer.android.com/guide/topics/manifest/manifest-intro?hl=ko