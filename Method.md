![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [메소드](#메소드)
- [오버로딩-Overloading](#오버로딩overloading)
- [오버라이딩-Overriding](#오버라이딩overriding)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 메소드(Method)
## 메소드
- 다른 언어에는 함수가 별도로 존재.  
  하지만 Java는 클래스를 떠나 존재할 수 없기 때문에 클래스 내에 존재  
  Java에선 이 함수를 메소드라고 칭함. (함수라는 표현은 잘 쓰지 않는다)
- 사용하는 이유?
  -	동일한 코딩이 반복될 때, 쓸데없는 코딩을 줄임
  -	전체적인 코드의 분석이 쉬워짐(코딩이 한결 깔끔해짐).
- Void를 제외한 변수형 메소드는 return을 해줘야 함.
- 기본 형태 :


 ```xml
public(혹은 다른 접근제어자) 반환할 자료형  메소드이명 (입력자료형1 입력변수1, 입력자료형2 입력변수2, …){       //이 때 입력 변수는 매개 변수라고 부를 수 있습니다.
    …
    return 반환할 값;  (메소드의 자료형이 void인 경우 return은 하지 않습니다.)
}
```


- 가변 인수(인자) : 동일 타입의 인수 여러 개를 축약형으로 입력  
  - 가변 인수는 마지막에 선언해야 한다.


 ```xml
public 리턴자료형 메소드명(입력자료형 …변수명){
    …
    return 리턴값;
}
```



 ```java
public static void main(String[] args) {
    System.out.println(add(3,5,7,9,10));
}
static int add(int ...number){
    int sum = 0;
    for(int i: number) sum +=i;
    return sum;
}

static void dynamicVariables(String s, String... ss){
    for(String a:ss)
        System.out.print(a+s);
}
```


## 오버로딩(Overloading)
- 동일한 이름의 메소드를 여러 개 생성. 매개변수의 타입이나 개수를 달리 함(**다중정의**)  
  이 때, 리턴 타입은 상관 없음
  


 ```java
public class OverloadingTest {
    public static void main(String[] args) {
        test(0);
        test(0, 0);
        test(0, "a");
        test(0, 0, 0);
    }

    static void test(int a) {
        System.out.println("테스트 1번");
    }

    static void test(int a, int b) {
        System.out.println("테스트 2번");
    }

    static void test(int a, String b) {
        System.out.println("테스트 3번");
    }

    static void test(int a, int b, int c) {
        System.out.println("테스트 4번");
    }
}
```
   
   
## 오버라이딩(Overriding)
- 상위 클래스에 속해 있는 메소드를 하위 클래스에서 호출할 때, **재정의**를 함  
(Interface와 함께 사용하는 경우가 많음)  



```java
class ParentTestClass {
    public int test = 5;
    public void print(){
        System.out.println(test);
    }
}

class ChildTestClass extends ParentTestClass{
    @Override
    public void print() {
        String test = "테스트 문장입니다.";
        super.print();
        System.out.println(test);
    }
}

public class OverridingTest {
    public static void main(String[] args) {
        ChildTestClass child = new ChildTestClass();
        child.print();
    }
}

```
