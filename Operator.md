![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [연산자](#연산자)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 연산자(Operator)
## 연산자
- 피 연산자의 개수에 따라 단항 연산자, 이항연산자, 삼항 연산자 로 나뉨
  - 최우선 연산자 
    - () : 우선 순위 변경  
  	- [] : 배열 표현
  - 단항 연산자   
    '~' : 1의 보수를 구해줌  
 	'!' : NOT ( true -> false )  
 	'+' : 문자열 이어주기 혹은 형식적 부호 (덧셈은 이후 산술연산자에서)  
 	'-' : 2의 보수 혹은 부호 바꿈  
 	'++' : 증가연산자 ( +1 )  
 	'--' : 감소연산자 ( -1 )  
  - 산술연산자 -> +, -, *, / : 사칙연산  
 	% : 나머지
  - Shift 연산자 -> << , >> : n비트만큼 이동 ( 8 >> 2 = 2 , 8 << 2 = 32)
  - 관계 연산자 -> return 값은 true or false.  
 		    <, >, <=, >= -> 두 값의 크기 비교  
 			==, != -> 두 값이 같은 지 여부 확인 ( 수치 혹은 null )
  - 논리 연산자  
  || : OR 결합. 둘 중 하나라도 만족 시 true  
 		  && : AND 결합. 둘 다 만족 시 true  
 		    | : OR 연산  (xml에서 주로 사용)
 		   & : AND 연산  
 		   ^ : XOR 연산   
  -	삼항 연산자 -> 조건 ? A : B  
 		   -> 조건 만족 시 A return, 조건 불만족 시 B return
  - 대입 연산자 -> =, +=, -=, *=, %=, /=, >>=, <<=  

```java
i = i + 3; 
i += 3;
// 위 두 식은 동일한 식
```