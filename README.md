---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Java?](#java)
- [Class? Instance?](#class-instance)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
# Java
## Java?
- 썬에서 최초 발표한 언어
- Oak 언어에서 시작해서 인터넷 프로그래밍 언어로 발전하면서 1995년 자바로 변경
- 이식형이 높은 언어. 서로 다른 실행환경을 가진 시스템에서도 사용 가능  
  (ex: C언어는 운영체제 종류에 따라 변수의 크기가 달라지지만 Java는 동일)
  - Java는 JVM(Java Virtual Machine) 환경에서 돌아가기 때문에 동일함
- 객체(Object)를 기반으로 하는 객체 지향 언어 (Class, Object, Method, Message 등 구성)로, 캡슐화, 상속성, 다형성을 완벽하게 지원
- 객체 지향 프로그래밍(OOP)?
  - 하나의 기능을 객체로 만들고, 객체들을 결합하는 프로그래밍 방법 
- C++은 메모리에 객체를 생성하고, 제거하기 위해 개발자가 직접 코드 작성
관리하지 않을 경우 프로그램이 불완전해지고 다운되는 현상 빈번히 발생
Java는 개발자가 아닌 Java에서 직접 메모리 관리
  - 객체 생성시 자동으로 메모리 영역을 찾아 할당, 사용 완료 시 가비지 컬렉터를 실행시켜 자동으로 객체 제거
  - 메모리 관리 용이
-	스레드 생성 및 제어, 다양한 api를 통해 스레드 관리가 쉬운 편
-	Java SE, Java EE, Java ME 등 다양한 에디션 존재
-	오픈 소스 언어이며, 사용량이 많기 때문에 라이브러리와 소스가 매우 많음  
구현을 위해 코드 작성을 따로 안해도 대부분의 기능이 온라인 상에 구현되어 있음
  - 유지보수가 쉽고 빠르게 진행 가능
- 단점
  - JVM을 통해 실행이 이루어지다보니 다른 언어에 비해 실행이 느림
  - 예외 처리를 일일히 지정해 줘야함
  - 코드의 길이가 다른 언어보다 긴 편
 
  

```java
package tew.class.test;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello");
    }
}


/**
* C계열 언어
*/
#include<stdio.h>
void main() { 
    puts("Hello");      // 혹은 cout << "Hello";
}
  
/**
* 파이썬
*/
print("Hello");

```





## Class? Instance?
- 같은 집단에 속하는 속성(attribute)과 행위(behavior)를 정의한 것으로 객체지향 프로그램의 기본적인 사용자 정의 데이터형(user defined data type)
- 클래스는 설계도(혹은 틀), 구현한 것이 인스턴스 같은 개념  <br/>
```java
    class Laptop {                                         // 클래스의 선언은 반드시 main 선두에 올 필요없음
        String name;                                       // field variable(멤버변수)
        String color;
        int value;
        boolean window;

        void webSurfing() {                                 // method(멤버함수) : 본체는 클래스 내부에 존재해야 함
            if (window) {
                System.out.println("Internet Explorer");
            } else {
                System.out.println("Safari");
            }
        }

        void stop() {
            System.out.println("시스템을 종료합니다.");
        }
    }

    public class JavaFirstClass {
        public static void main(String[] args) {
            Laptop laptop = new Laptop();              // 클래스는 참조형이므로 반드시 "new"연산자로 할당
            laptop.name = "맥북 프로";                 // new 연산자를 통해 Car객체를 생성 후 Car객체를
            laptop.color = "스페이스 그레이";          // 가리킬 수 있는 Car클래스 타입의 변수에 대입
            laptop.window = false;                    // 좌변은 객체를 만드는 틀, 우변은 틀에서 만들어진 물체
    
            laptop.webSurfing();                      // 객체의 필드나 메서드 접근 시 '.' 연산자 이용
            laptop.stop();
        }
    }
```
