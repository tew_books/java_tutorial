# Summary

* Java

---------------------------------------
* [Introduction. Java?](README.md)
* [Variables](Variables.md)
* [Array](Array.md)
* [Input & Output](InputOutput.md)
* [Operator](Operator.md)
* [Condition](Condition.md)
* [Loop](Loop.md)
* [Method](Method.md)
* [Class](Class.md)
* [Inheritance](Inheritance.md)
* [Abstract](Abstract.md)
* [AccessModifier](AccessModifier.md)
* [Thread](Thread.md)
* [Error&Exception](Error&Exception.md)


---------------------------------------

* Android

---------------------------------------
* [Gradle](Gradle.md)
* [Manifest](Manifest.md)
* [Activity](Activity.md)
* [View](View.md)
* [Layout](Layout.md)
* [Listener](Listener.md)
* [Fragment](Fragment.md)