![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [쓰레드](#쓰레드)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 쓰레드(Thread)

## 쓰레드

- 하나의 프로세스(동작 중인 프로그램) 내부에서 실행되는 하나의 흐름의 단위
  - 보통 한 개의 프로세스는 하나의 일만 함
  -	실행 중에 정지가 가능하며, 동시 수행이 가능
  - 프로세스 내 블록처리를 통해 시작점과 종료점을 가짐
  -	start() 메소드를 통해 쓰레드를 위한 공간을 생성 후 자동으로 run()을 호출
  -	run() 메소드가 종료되면 스레드도 종료
  -	한 번 종료한 스레드는 다시 시작할 수 없음
  -	스레드 내에서 다른 스레드를 강제로 종료할 수 없음
- 장점
  -	메모리 공유로 인한 시스템 자원 소모가 줄어듦
  -	다양한 수행 코드의 동시 수행이 가능해짐
- 단점
  -	자원 소모 중 충돌 발생 가능
  -	코딩이 복잡해짐 (사실 쓰레드 뿐만 아니라 코드가 길어지면 어쩔 수 없는 문제)
- extends 를 통해 상속, 불가능한 경우를 대비하여 Runnable 존재
  -	Runnable은 run() 메소드만 존재
	
  
```java
public class ThreadTest extends Thread {
    int number;

    public ThreadTest(int number) {
        this.number = number;
    }
    public void run() {
        System.out.println(System.currentTimeMillis() + " " + this.number+" thread started");
        try {
            Thread.sleep(500);
        }catch(Exception e) {

        }
        System.out.println(System.currentTimeMillis() + " " + this.number+" thread ended");
    }

    public static void main(String[] args) {
        for(int i=0; i<10; i++) {
            Thread t = new ThreadTest(i);
            t.start();
        }
        System.out.println("main ended");                //  main 메소드 종료 시 "main ended"를 출력
    }
}
```


- *본인 방금 Thread가 호출이 다 종료되면 "main ended" 출력되는 상상함*



```xml
1575355814054 0 thread started
1575355814054 5 thread started
1575355814054 4 thread started
1575355814054 3 thread started
1575355814054 2 thread started
1575355814054 1 thread started
1575355814055 8 thread started
1575355814055 9 thread started
main finished
1575355814055 7 thread started
1575355814054 6 thread started
1575355814555 0 thread ended
1575355814555 5 thread ended
1575355814556 1 thread ended
1575355814556 2 thread ended
1575355814556 8 thread ended
1575355814556 9 thread ended
1575355814556 4 thread ended
1575355814556 7 thread ended
1575355814556 3 thread ended
1575355814556 6 thread ended
```

- *하지만 어림도 없지*

  - 동시에 작업이 진행되기 때문에 순서에 상관없이 마구 출력됨
  - join() 등으로 대처 가능, 이 후에 안드로이드에서 다시 설명할 예정