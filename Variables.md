![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [변수](#변수)
- [전역변수vs지역변수](#전역변수-vs-지역변수)
- [변수 명명 방법](#변수-명명-방법-case-naming-convention)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# __변수(Variable) 및 자료형__

## 변수
  - 값을 저장할 수 있는 메모리 공간 = 변수  
    변수를 사용하기 위해서는 ‘선언’이라는 절차가 필요  
    -> 변수를 선언한다는 것은 메모리 공간을 컴퓨터 혹은 디바이스에 만든다는 것  
    이 때, 선언 후에는 반드시 값을 초기화 해주어야 함(일명 저장)  


```java
String name;
String color;
boolean window;
```

- 변수 타입을 지정해줄 때, 대소문자 구분은 확실히! (Int x, int o , String o, string x, boolean o, Boolean o ????? Boolean은 참조형, boolean은 자료형)
- 초기화 하지 않는 경우 int, long : 0, float, double : 0.0, String : null, boolean : false
- 언어마다 사용하는 기본적으로 제공되는 자료형의 이름은 조금씩 다름

```xml
        타입 - 참조형 : 클래스, 인터페이스, 배열, 열거형
             - 기본형 ----- 진위형 : boolean
                  └-------- 수치형 ----- 정수형 : byte, short, int, long, char
                               └------------ 실수형 : float, double
```
- Java의 경우 아래와 같이 구분.
  - 문자형 : C와 동일하게 String 타입 선언 가능,  char : 문자 1개를 저장하는 자료형  
  - 진위형 : boolean 타입 선언을 통해 true & false 선언  
  - 정수형 : byte,   short,   int,   long 타입 선언을 통해 정수형 변수 선언  
       각기  1Byte   2Byte   4Byte   8Byte 의 사이즈를 가짐  
       byte, short는 int보다 적은 Bit를 차지하지만 JVM은 4Byte단위로 저장하기 때문에  
       int보다 작은 자료형의 값을 계산 시 int 형으로 형 변환되어 연산을 수행함 -> int를 쓰자  
       (숫자 중에서도 정수는 대부분 int형을 사용하게 됩니다.)  
  - 실수형 : double이 기본 자료형. float는 실수 뒤에 f를 붙여줘야 함  
            ex) pi = 3.14 -> pi는 double형, pi = 3.14f -> pi는 float형  
    double이 float보다 약 두배의 정밀도를 가짐
  - 변수를 사용하는 이유?  
    간단한 프로그래밍은 숫자, 문자 값을 바로 입력해서 계산이 가능하지만, 계산식이 점점 복잡해지는 경우(고차원화) 처음부터 다시 계산을 수행해야 함

변수를 사용하지 않는 경우
```java
        System.out.println("1 + 2 = " + (1 + 2));
        System.out.println("1 + 2 + 3 = " + (1 + 2 + 3));
        System.out.println("1 + 2 + 3 + 4 = " + (1 + 2 + 3 + 4));
        System.out.println("1 + 2 + 3 + 4 + 5 = " + (1 + 2 + 3 + 4 + 5));
```

변수를 사용하는 경우

```java
        int value = 1 + 2;
        System.out.println("1 + 2 = " + value);
        value += 3;
        System.out.println("1 + 2 + 3 = " + value);
        value += 4;
        System.out.println("1 + 2 + 3 + 4 = " + value);
        value += 5;
        System.out.println("1 + 2 + 3 + 4 + 5 = " + value);
 ```
 
## 전역변수 vs 지역변수  
- 전역변수 : 함수 밖에서 선언하며 어디서든 호출하면 사용할 수 있는 변수  
  - 객체(instance) 변수와 클래스(static) 변수로 나눠짐  
    - 객체 변수 : 클래스영역에서 선언, 객체 생성 시 만들어 짐  
    - Static 변수 :  
      메모리에 고정적으로 할당되어 프로그램이 종료될 때 해제   
      Static method 내부에는 static 변수만 사용 가능
- 지역변수 : 주로 함수 혹은 조건문 안에 생성되어 그 지역에서만 사용할 수 있는 변수  
  - 해당 함수 혹은 조건문 외에서는 호출 및 사용 불가능.
- 상수 필드(final)
  - 변수형 앞에 ‘final’ 을 붙여서 상수필드 지정, 실행 중에는 수정할 수 없는 값.
  - 상수는 static final 변수형 변수명 = 초기값 (변수명은 Snake Case with all caps)  

```java
    final String stName = "UserName";  // 선언 시 초기화 필요
    final String stGender;
    int age;

    public FinalFieldTest(String stGender, int age) {
        this.stGender = stGender;
        this.age = age;
    } // 이처럼 생성자에서 초기화를 해주는 경우에는 필드 선언 시 초기화 안해줘도 됨
```


## 변수 명명 방법 Case naming convention  
  -	Lower Camel Case
    - lowerCamelCase, stName, userProperty 와 같이 첫 문자는 소문자, 이후 각 단어의 첫 문자를 대문자로 표기.
    - 주로 변수명에 사용
  - Upper Camel Case (or Pascal Case)
    - UpperCamelCase, MainActivity, UserInterface 와 같이 각 단어의 첫 문자를 대문자로 표기
    - Class, Interface 명 등에 사용<br/>
  -	Snake Case (with All Caps)
    - snake_case, activity_main, dialog_user_setting 과 같이 모든 단어는 소문자로, 단어는 언더바로 구분
    - 주로 resource 명이나 database 필드명으로 사용
    - SNAKE_CASE, PREF_USER_LOGIN_ID, PERMISSION_WRITE_EXTERNAL_STORAGE 와 같이 모든 단어를 대문자로, 동일하게 단어는 언더바(‘_’)로 구분
    - 주로 상수 선언에 사용<br/>
  -	Hungarian Notation
    - nNumber, stName, fSomethingFloat, isStart 등
    - 변수에 따라 변수명 앞에 다른 접두사를 붙임<br/>
  -	Kebab Case
    - lesson-with-case-naming-convention 과 같이 단어의 연결을 하이픈(‘-‘)으로 표시
    - 주로 URL에 사용 (Java Android에선 거의 쓸 일 없음)
