![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Java Guide

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [View](#view)
- [ViewGroup](#viewgroup)
- [TextView](#textview)
- [EditText](#edittext)
- [ImageView](#imageview)
- [Button](#button)
- [ImageButton](#imagebutton)
- [CheckBox](#checkbox)
- [Guideline](#guideline)
- [ScrollView](#scrollview)
- [RecyclerView](#recyclerview)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# View
## View
- View : 어플리케이션에서 눈에 가시적으로 표현되는 모든 것
  - View 클래스는 모든 뷰들의 최상위 클래스(부모 클래스)
  - View 는 자신이 화면 어디에 그려져야 하는지 정보가 없음  
    화면에 배치하기 위해 필요한 것이 ViewGroup 혹은 Container 필요.
  - View의 순서에 따라 z-order(화면과의 수직 방향)이 바뀌기 때문에 순서도 중요함
  - 공통적으로 자주 사용되는 View의 특성


```xml
    android:id="@+id/viewName"
    android:layout_width="##dp"
    android:layout_height="##dp"
    android:layout_margin="##dp"
    android:padding="##dp"
    android:background="@drawable/XXXXXXXXXXXXX"
    android:gravity="center"
    android:layout_gravity="center"
    android:theme="@style/XXXXXXXXXX"
    app:layout_constraintStart_toStartOf="parent"
    ...
```


  - id : view의 이름  
    width, height : View의 너비와 높이. 필수 값  
    margin : 연결된 View로부터의 거리(외부 여백)  
    padding : View 내부의 여백  
    background : View의 배경  
    gravity : View의 내용 정렬 방식(가운데 정렬, 왼쪽 정렬 등등..)  
    layout_gravity : child View의 정렬 방식  
    theme : View의 스타일을 설정할 때 사용 (주로 커스터마이징 용)
  - width 및 height의 사이즈 :  
    match_parent : parent layout의 사이즈와 동일하게  
    wrap_content : View가 갖는 내부 속성의 사이즈에 딱 맞게  
    0dp : 연결된 뷰들의 위치에 따라 크기를 다르게 설정
  - 사이즈 설정  
    - px : 디바이스의 스크린마다 다르게 들어가는 실제 픽셀. 기존에는 1dp = 1px였으나 해상도가 큰 디스플레이들이 나오면서 의미하는 값이 달라짐.
    - dp : Density independent pixels. 장치의 해상도 밀도에 상관없이 거의 동일한 크기를 가짐
    - sp : Scale independent pixels. 글꼴 크기를 지정할 때 주로 사용


![view](https://user-images.githubusercontent.com/47145871/71087079-a8ff9580-21de-11ea-84c6-e61618547dd6.png)


참고 : https://developer.android.com/reference/android/view/View
## ViewGroup
- ViewGroup : View와는 다르게 가시적으로 표현되지는 않지만, 뷰를 배치하거나 그룹핑하는 역할
  - View의 하위 클래스
  - Children이라 부르는 다른 View들을 담는 특수한 View
  - layout 과 view container의 기본 클래스로 사용됨
  - ViewGroup.LayoutParams 라는 클래스를 통해 layout의 파라미터를 다룸


## TextView
- TextView : 화면에 텍스트를 표시하는 View
  - 가장 많이 사용되는 위젯(사용자가 상호 작용하는 인터페이스 요소)
  - Layout 리소스 xml에 TextView 추가


```xml
<TextView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    ...
    android:text="XXXXXXXX"
    android:textSize="##sp"
    android:fontFamily="@fonts/XXXXXXXX.ttf"
    android:textAllCaps="false"
    android:textColor="#222222"
    android:textStyle="bold"
    android:textColorHint="#000000"
    android:hint="힌트문구"
    ... />
    
```


  - text : TextView에 들어갈 text 입력  
    textSize : 입력될 텍스트의 크기  
    fontFamily : 임의의 폰트를 사용할 때 설정  
    textAllCaps : 입력되는 텍스트를 모두 대문자로 하고 싶은 경우 true 설정  
    textColor : 입력되는 텍스트의 색상 (#FFFFFF 와 같은 RGB 로 설정)  
    textStyle : normal(기본), bold(굵게), italic(기울어짐), bold_italic(굵고 기울어지게)  
    hint : 텍스트가 입력된 값이 없는 경우 제공되는 힌트  
    textColorHint : 텍스트가 입력된 값이 없는 경우 제공되는 힌트의 색상 (#FFFFFF 와 같은 RGB 로 설정)  
  - java소스에서는 set 명령어를 통해 설정 가능 (textView.setText("예시 문구");, textView.setTextSize(15); 등)


```java
    TextView textView = findViewById(R.id.textView);
    textView.setText("예시 문구");
```


## EditText
- EditText : TextView의 하위 클래스이며, 텍스트를 입력할 수 있는 View
  - Layout 리소스 xml에 EditText 추가
  - TextView와 대부분 동일한 속성을 지니지만 TextView 보다 EditText에서 주로 hint, textColorHint 속성 사용
  - 입력된 데이터는 java 소스에서 editText.getText().toString(); 으로 String 값을 가져옴
  - 색상 변경을 할 때에는 아래처럼 style을 선언해주고, theme 속성으로 가져와야 함
  - singleLine : true, false를 통해 줄바꿈 가능여부 처리. 현재는 deprecated 되어서 maxLines = "1" 로 처리하게 됨
  - imeOptions : 보통 키보드의 완료 버튼 (엔터) 의 액션을 정해주는데 사용합니다.
    - actionDone, actionNext, actionSend 등
    - java 단에서 OnEditorActionListener를 통해 actionId 를 받고, 그 값을 이용해 추가적인 이벤트 설정 가능
  - inputType : EditText에 입력을 할 text의 타입 설정. 기본 설정은 text
    - none, number, numberDecimal, textPassword, text, textEmailAddress, numberPassword, date, datetime 등
    - 설정에 따라 키보드(softKeyboard)의 타입이 바뀜



```xml
<EditText
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    ...
    android:text="XXXXXXXX"
    android:textSize="##sp"
    android:fontFamily="@fonts/XXXXXXXX.ttf"
    android:textAllCaps="false"
    android:textColor=""
    android:textStyle=""
    android:textColorHint=""
    android:hint=""
    android:maxLines="1"
    android:inputType="text"
    android:imeOptions="actionDone"
    ... />

//style
<style name="CustomEditText" parent="Widget.AppCompat.EditText">
    <item name="android:colorAccent">#348920</item>
    <item name="android:colorControlNormal">#459A31</item>
    <item name="android:colorControlHighlight">#23671F</item>
    <item name="android:colorControlActivated">#123456</item>
</style>
```


```java
    EditText editText = findViewById(R.id.editText);
    editText.setText("예시 문구");
    String sentece = editText.getText().toString();
```



## ImageView
- ImageView : 주로 화면에 이미지를 표시하기 위해 사용
  - Layout 리소스 xml에 ImageView 추가
  - 주로 png 또는 jpg 형식의 파일을 사용  
    이미지는 한글, 대문자, 숫자로 시작 하는 파일명 사용 불가. (ex> 이미지.png (x), Image.png (x), 3rd_image.png (x), image3rd.png (O), _3image.png(O) )
  - java소스에서는 setImageResource(R.drawable.XXXXXXXX); 로 이미지 설정 (이후 Picasso Library로 다룰 예정)
  - https://material.io/resources/icons/?style=baseline 에서 기본적인 아이콘을 얻을 수 있음
  - 컬러 매치는 https://material.io/resources/color/#!/?view.left=0&view.right=0 에서 참고 가능


```xml
<ImageView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    ...
    android:src="@drawable/XXXXXXXX"
    android:scaleType="center"
    ... />
```


  - src : 이미지뷰에 들어갈 이미지 혹은 색상 입력  
    scaleType : 
    - matrix : 이미지의 매트릭스 화. 패턴의 반복
    - fitXY : ImageView의 사이즈에 맞도록 이미지를 스케일링
    - fitStart : 이미지를 너비와 높이 중 작은 사이즈의 기준에 맞게 스케일링  
    View의 Start를 기준으로 이미지 표현.
    - fitCenter : 이미지를 너비와 높이 중 작은 사이즈의 기준에 맞게 스케일링  
    View의 Center를 기준으로 이미지 표현
    - fitEnd : 이미지를 너비와 높이 중 작은 사이즈의 기준에 맞게 스케일링  
    View의 End를 기준으로 이미지 표현
    - center : 이미지를 가운데 표시(스케일은 X)
    - centerCrop : 이미지 비율을 유지하며 스케일. ImageView를 벗어나면 잘라냄
    - centerInside : 이미지 비율을 유지하며 스케일. ImageView를 벗어나지 않음


```java
    ImageView imageView = findViewById(R.id.imageView);
    imageView.setImageResource(R.drawable.example_image);
```


## Button
- Button : 주로 클릭형 액션 및 이펙트를 줄 때 사용
  - background 속성을 통해 Button 커스터마이징 가능
  - java 소스에서 button은 주로 setOnClickListener 외에는 사용하지 않음


```xml
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_pressed="true" android:drawable="#FF0000" />
    <item android:state_focused="true" android:drawable="#00FF00" />
</selector>
```


```java
Button button = findViewById(R.id.button);
button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
    
    }
});
```


## ImageButton
- ImageButton : Button 위에 이미지를 출력하여 사용자가 Button의 외형을 커스터마이징
  - ImageView처럼 src에 값을 입력하여 외형 처리



```xml
<ImageButton
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:id="@+id/imageButton1"
        android:padding="0dp"
        android:src="@drawable/img"/>
```


## CheckBox
- CheckBox : 클릭 이벤트에 따라 checked unchecked 결과 값을 갖는 Button 상속형 (정확히는 CompoundButton 상속) View
  - xml 소스에선 text를 통해 문구 설정  
    checked = "true" or "false" 를 통해 체크 여부 설정
  - java 소스에선 isChecked()를 통해 boolean 값 반환
  



```xml
<CheckBox
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="체크박스"
        android:id="@+id/checkBox"/>
```



```java
CheckBox checkBox;
boolean result = checkBox.isChecked();
```



## Guideline
- Guideline : Constraint Layout에서 디바이스의 크기 별 화면사이즈 변화에 대응하기 위해 퍼센트 값으로 기준을 주는 것
  - Constraint Layout 외에는 사용 불가
  - xml 소스에서 주로 처리
  - View 연결 및 구성을 위해 사용



```xml
<androidx.constraintlayout.widget.Guideline
    android:id="@+id/guideline"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:orientation="vertical"
    app:layout_constraintGuide_percent="0.5"
    />
    
    
    
    <TextView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    ...
    app:layout_constraintStart_toEndOf="@+id/guideline"
    ... />
<TextView
    
```


## ScrollView
- ScrollView : 화면 내에 스크롤 기능이 필요한 경우 넣음
  - ScrollView 의 child는 단 하나만 가능  
    그렇기 때문에 보통 ScrollView를 최상단에 넣고, child로 ConstraintLayout을 넣어 화면을 구성  
    2개 이상 넣으면 해당 Exception 발생
    이 때 ConstraintLayout의 height는 wrap_content를 사용하는 것을 추천



```xml
java.lang.IllegalStateException: ScrollView can host only one direct child
```


```xml
<ScrollView
    xmlns:android="http://schemas.android.com/apk/res/android" 
    xmlns:tools="http://schemas.android.com/tools" 
    android:id="@+id/scrollView" 
    android:layout_width="match_parent"
    android:layout_height="match_parent"> 
    
    <androidx.constraintlayout.widget.ConstraintLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity">
    
    ...
    
    </androidx.constraintlayout.widget.ConstraintLayout>
    
</ScrollView>


```



## RecyclerView
- RecyclerView : 차후 설명 예정


